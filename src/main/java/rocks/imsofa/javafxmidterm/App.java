package rocks.imsofa.javafxmidterm;

import java.io.File;
import java.io.IOException;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 * JavaFX App
 */
public class App extends Application {

    private Group imageContainer = null;
    private Text text=null;

    @Override
    public void start(Stage stage) {
        VBox vbox = new VBox();
        HBox hbox = new HBox();
        //1. (20%) 建立三個 radio button，讓 user 從中選擇 Grinch, Pikachu, Simpson 其中一個（只能選擇一個），預設將 Grinch 選起來
        ToggleGroup group1 = new ToggleGroup();
        RadioButton buttonGrinch = null;
        RadioButton buttonPikachu = null;
        RadioButton buttonSimpson = null;
        //////////////////////////////////////////////////////////////////////////////////////////////

        
        //2. (20%) 將專案中的 Grinch.jpg, Pikachu.png, Simpson.jpg 讀入對應的 image 中，並顯示到 imageView 上，
        //將 image 縮放到適當的大小並保持比例
        Image imageGrinch = new Image("");
        Image imagePikachu = new Image("");
        Image imageSimpson = new Image("");
        ImageView imageView = new ImageView(imageGrinch);
        ///////////////////////////////////////////////////////////////////////////////////////////
        hbox.getChildren().addAll(buttonGrinch, buttonPikachu, buttonSimpson);
        imageContainer = new Group();
        imageContainer.getChildren().add(imageView);
        vbox.getChildren().addAll(hbox, imageContainer);
        
        //3. (20%) 利用 selectedToggleProperty 來切換 image
        
        ///////////////////////////////////////////////

        Circle circle = new Circle(500, 150, 100);
        circle.setStroke(Color.BLUE);
        circle.setFill(Color.WHITE);

        Circle circle1 = new Circle(400, 180, 10);
        circle1.setStroke(Color.BLUE);
        circle1.setFill(Color.WHITE);

        Circle circle2 = new Circle(380, 190, 10);
        circle2.setStroke(Color.BLUE);
        circle2.setFill(Color.WHITE);

        imageContainer.getChildren().addAll(circle, circle1, circle2);
        text = new Text("123");
        imageContainer.getChildren().add(text);
        text.setX(450);
        text.setY(150);

        HBox southPanel = new HBox();
        southPanel.setAlignment(Pos.CENTER);
        TextField textField = new TextField();
        Button button = new Button("Say");
        Button saveButton = new Button("Save");
        southPanel.getChildren().addAll(textField, button, saveButton);
        
        //4. (20%) 在使用者按下 Say 按鈕後，呼叫 updateSay 將 textField 裏面的文字傳送過去
        
        //////////////////////////////////////////
        
        //5. (20%) 在使用者按下 Save 按鈕後，呼叫 saveToFile 將目前的圖片存檔到 output.png
        
        /////////////////////////////////////////////////////////////////////
        
        //bonus: 按下 Save 後出現FileChooser讓使用者選擇檔名，額外得到 10%，但總分不超過 100

        vbox.getChildren().add(southPanel);
        vbox.setSpacing(20);

        var scene = new Scene(vbox, 800, 600);
        stage.setScene(scene);
        stage.show();
    }
    
    private void updateSay(String str){
        text.setText(str);
    }

    private void saveToFile(File file) {
        try {
            SnapshotParameters param = new SnapshotParameters();
            param.setDepthBuffer(true);
            WritableImage snapshot = imageContainer.snapshot(param, null);
            ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", new File("output.png"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch();
    }

}
