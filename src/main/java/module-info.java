module rocks.imsofa.javafxmidterm {
    requires javafx.controls;
    requires javafx.swing;
    requires java.desktop;
    exports rocks.imsofa.javafxmidterm;
}
